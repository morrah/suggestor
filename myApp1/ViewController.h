//
//  ViewController.h
//  myApp1
//
//  Created by helL on 9/1/17.
//  Copyright © 2017 helL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *MainImage;
@property (weak, nonatomic) IBOutlet UIButton *GetImgBtn;
@property (weak, nonatomic) IBOutlet UIButton *RetakeBtn;

@end

