//
//  ViewController.m
//  myApp1
//
//  Created by helL on 9/1/17.
//  Copyright © 2017 helL. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end

@implementation ViewController

bool debug = false;
NSString *apiUrl = @"http://pub-web-api.service.skoda.kjiactep:8887/api/v0/offers/search";
//NSString *apiUrl = @"http://192.168.1.4:80/tsapi";
int suggestionsLimit = 10;
int suggestionImgSize = 100;
int suggestionImgSpace = 10;
NSString *placeholderImage = @"Image-1";
int bottomPanelHeight = 80;
int topPanelHeight = 50;
CGRect screenBounds;
UIImagePickerController *pickerCtrl;
UIScrollView *suggestionsView;

bool modalIsShowing;
bool readyToProcess;
NSMutableURLRequest *apiRequest;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.clipsToBounds = YES;
    screenBounds = self.view.bounds;
    CGRect mainImgFrame = screenBounds;
    mainImgFrame.size.height = mainImgFrame.size.height - topPanelHeight - bottomPanelHeight;
    mainImgFrame.origin.y = topPanelHeight;
    self.MainImage.frame = mainImgFrame;
    pickerCtrl = [[UIImagePickerController alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
    printf("ViewDidAppear\n");
    [self showPickerIfPossible];
}

- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(nonnull NSDictionary<NSString *,id> *)info {
    printf("didFinishPickingMediaWithInfo\n");
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.MainImage.image = image;
    readyToProcess = true;
    [pickerCtrl dismissViewControllerAnimated:YES completion:nil];
    printf("hide photo picker\n");
    modalIsShowing = false;
}

- (IBAction)GetBtnTap:(id)sender {
    printf("GetBtnTap Tapped!\n");
    readyToProcess = true;
    [self fetchAndProcessSuggestionsWithLimit:suggestionsLimit withPhrase:[self recognizeTextFromImage:self.MainImage.image] ];
    readyToProcess = false;
}

- (IBAction)RetakeBtnTap:(id)sender {
    printf("RetakeBtnTap Tapped!\n");
    readyToProcess = false;
    [self showPickerIfPossible];
}

- (void)showPickerIfPossible {
    printf("showPickerIfPossible\n");
    if (!modalIsShowing && !readyToProcess) {
        if ([UIImagePickerController isSourceTypeAvailable:(UIImagePickerControllerSourceTypeCamera)]) {
            printf("try to take the pict\n");
            pickerCtrl.delegate = self;
            pickerCtrl.sourceType = UIImagePickerControllerSourceTypeCamera;
            printf("will take a pict right now\n");
            //TODO make it automatic, see for ex. [pickerCtrl takePicture];
            [self presentViewController:pickerCtrl animated:YES completion:nil];
            printf("showing photo picker\n");
            modalIsShowing = true;
        } else {
            printf("No camera available\n");
        }
    }
}

- (void)processSuggestionsResponse:(NSData*)jsonData {
    NSError *error;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error ];
    if (error) {
        NSLog(@"%@", error.localizedFailureReason);
        NSLog(@"Error: could not parse JSON!\n");
        return;
    }
    if (![jsonObject isKindOfClass:[NSObject class]]) {
        printf("Error: expected object but another type received!\n");
        return;
    }
    NSDictionary *resultJsonDict = (NSDictionary *) jsonObject;
    NSArray *data = (NSArray *)[(NSDictionary *)[resultJsonDict valueForKey:@"results"] valueForKey:@"data"];
    if (!data) {
        printf("No data in response!\n");
        return;
    }
    printf("Data present\n");
    [self prepareSggestionsView];
    int pozXOffset = -1 * suggestionImgSize;
    for (int i = 0; i < [data count]; i++) {
        pozXOffset = pozXOffset + suggestionImgSize + suggestionImgSpace;
        NSDictionary *imagesDict = [(NSDictionary *)[(NSDictionary *)[data objectAtIndex:i] objectForKey:@"product"] objectForKey:@"images"];
        NSString *firstKey;
        NSString *imgUrlStr;
        NSArray *imgKeys = [imagesDict allKeys];
        if ([imgKeys count] > 0) {
            firstKey = [imgKeys objectAtIndex:0];
        }
        printf("firstKey: %s\n", [firstKey UTF8String]);
        if (firstKey) {
            imgUrlStr = [imagesDict objectForKey:firstKey];
        }
        if (imgUrlStr) {
            printf("img url %s\n", [imgUrlStr UTF8String]);
            NSURL *imgUrl = [NSURL URLWithString:imgUrlStr];
            NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:imgUrl completionHandler:^(NSData * imgData, NSURLResponse * response, NSError * error) {
                if (error != NULL) {
                    printf("Error: img fetch: %s", [[error description] UTF8String]);
                    return;
                }
                [self addItem:imgData ToView:suggestionsView fromPosX:pozXOffset];
            }];
            [task resume];
        }
        else {
            printf("No img url\n");
            [self addItem:NULL ToView:suggestionsView fromPosX:pozXOffset];
        }
    }
    [self showSuggestionsView];
    [self.view setNeedsDisplay];
}

- (void)prepareSggestionsView {
    if (!suggestionsView) {
        printf("Create a view\n");
        CGRect suggestionsViewFrame = screenBounds;
        suggestionsViewFrame.size.height = suggestionImgSize + 2 * suggestionImgSpace;
        suggestionsViewFrame.origin.y = self.MainImage.frame.origin.y + self.MainImage.frame.size.height - suggestionsViewFrame.size.height;
        suggestionsView = [[UIScrollView alloc] initWithFrame:suggestionsViewFrame];
        [suggestionsView setBackgroundColor:[UIColor colorWithRed:.5 green:.5 blue:.5 alpha:.3]];
    }
    if ([suggestionsView isDescendantOfView:(UIView *)self.view]) {
        printf("Remove suggestions view\n");
        [suggestionsView removeFromSuperview];
    }
    for (UIView *subview in suggestionsView.subviews) {
        @autoreleasepool {
            if ([subview isKindOfClass:[UIImageView class]]) {
                [subview removeFromSuperview];
                printf("subview removed\n");
            }
        }
    }
}

- (void) showSuggestionsView {
    if (![suggestionsView isDescendantOfView:(UIView *)self.view]) {
        printf("Add suggestions view\n");
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [self.view addSubview:suggestionsView];
        });
    }
    else {
        printf("suggestions view already exists in main view!!!\n");
    }
}

- (void)addItem:(nullable NSData*)imgData ToView:(UIScrollView*)suggestionsView fromPosX:(int)pozXOffset {
    CGRect imgFrame = CGRectMake(pozXOffset, suggestionImgSpace, suggestionImgSize, suggestionImgSize);
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:imgFrame];
    [imgView setBackgroundColor:[UIColor colorWithRed:0 green:.1 blue:0 alpha:.5]];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    [imgView setImage:[self getImg:imgData]];
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [suggestionsView addSubview:imgView];
        NSUInteger subviewsCount = [suggestionsView.subviews count];
        suggestionsView.contentSize = CGSizeMake(subviewsCount * (suggestionImgSize + suggestionImgSpace), suggestionsView.frame.size.height);
        printf("subview added at posXOffset %d, width %f, subviewsCount %d\n", pozXOffset, suggestionsView.contentSize.width, (int)subviewsCount);
    });
}

- (UIImage*)getImg:(nullable NSData*)imgData {
    UIImage *img;
    if (imgData == NULL) {
        printf("ImgData is NULL, change to placeholder!!!\n");
        img = [self getPlaceholderImage];
        return img;
    }
    img = [UIImage imageWithData:imgData];
    if (img == NULL) {
        printf("img is NULL, change to placeholder!!!\n");
        img = [self getPlaceholderImage];
    }
    return img;
}

- (UIImage*) getPlaceholderImage {
    return [UIImage imageNamed:placeholderImage inBundle:NULL compatibleWithTraitCollection:NULL];
}

- (NSString*)recognizeTextFromImage:(UIImage*)srcImg {
    //TODO chain a recognizer
    return @"knob";
}

//TODO add a search phrase as second parameter
- (void)fetchAndProcessSuggestionsWithLimit:(int)limit withPhrase:(NSString*)phrase {
    printf("getSuggestions: limit %d, phrase %s\n", limit, [phrase UTF8String]);
    if (debug) {
        NSString *ret = @"{\"results\":{\"total\":37,\"data\":[{\"id\":\"9\",\"price\":{\"currency\":\"USD\",\"value\":2.80},\"product\":{\"title\":\"Knob9\",\"description\":\"Knob descr 9\",\"identifiers\":{},\"attributes\":{},\"images\":{\"1\":\"https://wallpaperbrowse.com/media/images/pictures-1.jpg\",\"2\":\"https://wallpaperbrowse.com/media/images/pictures-2.jpg\"},\"commodity\":{\"code\":\"31162801\",\"name\":\"Handles or knobs\",\"service\":false,\"parent\":{\"code\":\"31162800\",\"name\":\"Miscellaneous hardware\",\"service\":false,\"parent\":{\"code\":\"31160000\",\"name\":\"Hardware\",\"service\":false,\"parent\":{\"code\":\"31000000\",\"name\":\"Manufacturing Components and Supplies\",\"service\":false,\"parent\":null}}}},\"UOM\":\"psc.\"},\"supplierId\":\"9ca21bc1-9a17-464f-8bbd-86a15d08277f\",\"supplierName\":\"crow0002\",\"supplierPublic\":false},{\"id\":\"11\",\"price\":{\"currency\":\"USD\",\"value\":1.20},\"product\":{\"title\":\"Knob1\",\"description\":null,\"identifiers\":{},\"attributes\":{\"Other\":\"Knob1 other\"},\"images\":{\"main\":\"https://wallpaperbrowse.com/media/images/pictures-2.jpg\"},\"commodity\":{\"code\":\"31162801\",\"name\":\"Handles or knobs\",\"service\":false,\"parent\":{\"code\":\"31162800\",\"name\":\"Miscellaneous hardware\",\"service\":false,\"parent\":{\"code\":\"31160000\",\"name\":\"Hardware\",\"service\":false,\"parent\":{\"code\":\"31000000\",\"name\":\"Manufacturing Components and Supplies\",\"service\":false,\"parent\":null}}}},\"UOM\":null},\"supplierId\":\"a2d078ed-6c61-4c6c-b3d9-dadd42f193b8\",\"supplierName\":\"crow0001\",\"supplierPublic\":true},{\"id\":\"14\",\"price\":{\"currency\":\"USD\",\"value\":2.40},\"product\":{\"title\":\"Knob4\",\"description\":null,\"identifiers\":{},\"attributes\":{\"Other\":\"Knob4 other\"},\"images\":{\"3\":\"https://wallpaperbrowse.com/media/images/pictures-3.jpg\"},\"commodity\":{\"code\":\"31162801\",\"name\":\"Handles or knobs\",\"service\":false,\"parent\":{\"code\":\"31162800\",\"name\":\"Miscellaneous hardware\",\"service\":false,\"parent\":{\"code\":\"31160000\",\"name\":\"Hardware\",\"service\":false,\"parent\":{\"code\":\"31000000\",\"name\":\"Manufacturing Components and Supplies\",\"service\":false,\"parent\":null}}}},\"UOM\":\"dz\"},\"supplierId\":\"a2d078ed-6c61-4c6c-b3d9-dadd42f193b8\",\"supplierName\":\"crow0001\",\"supplierPublic\":true},{\"id\":\"12\",\"price\":{\"currency\":\"USD\",\"value\":2.00},\"product\":{\"title\":\"Knob2\",\"description\":null,\"identifiers\":{},\"attributes\":{\"Other\":\"Knob2 other\"},\"images\":{\"1\":\"https://wallpaperbrowse.com/media/images/pictures-4.jpg\"},\"commodity\":{\"code\":\"31162801\",\"name\":\"Handles or knobs\",\"service\":false,\"parent\":{\"code\":\"31162800\",\"name\":\"Miscellaneous hardware\",\"service\":false,\"parent\":{\"code\":\"31160000\",\"name\":\"Hardware\",\"service\":false,\"parent\":{\"code\":\"31000000\",\"name\":\"Manufacturing Components and Supplies\",\"service\":false,\"parent\":null}}}},\"UOM\":\"kg\"},\"supplierId\":\"a2d078ed-6c61-4c6c-b3d9-dadd42f193b8\",\"supplierName\":\"crow0001\",\"supplierPublic\":true},{\"id\":\"5\",\"price\":null,\"product\":{\"title\":\"Knob5\",\"description\":\"Knob descr 5\",\"identifiers\":{},\"attributes\":{},\"images\":{\"1\":\"https://wallpaperbrowse.com/media/images/pictures-5.jpg\"},\"commodity\":{\"code\":\"31162801\",\"name\":\"Handles or knobs\",\"service\":false,\"parent\":{\"code\":\"31162800\",\"name\":\"Miscellaneous hardware\",\"service\":false,\"parent\":{\"code\":\"31160000\",\"name\":\"Hardware\",\"service\":false,\"parent\":{\"code\":\"31000000\",\"name\":\"Manufacturing Components and Supplies\",\"service\":false,\"parent\":null}}}},\"UOM\":\"psc.\"},\"supplierId\":\"9ca21bc1-9a17-464f-8bbd-86a15d08277f\",\"supplierName\":\"crow0002\",\"supplierPublic\":false},{\"id\":\"99\",\"price\":{\"currency\":\"USD\",\"value\":2.80},\"product\":{\"title\":\"Knob9\",\"description\":\"Knob descr 99\",\"identifiers\":{},\"attributes\":{},\"images\":{\"1\":\"https://wallpaperbrowse.com/media/images/pictures-1.jpg\",\"2\":\"https://wallpaperbrowse.com/media/images/pictures-6.jpg\"},\"commodity\":{\"code\":\"31162801\",\"name\":\"Handles or knobs\",\"service\":false,\"parent\":{\"code\":\"31162800\",\"name\":\"Miscellaneous hardware\",\"service\":false,\"parent\":{\"code\":\"31160000\",\"name\":\"Hardware\",\"service\":false,\"parent\":{\"code\":\"31000000\",\"name\":\"Manufacturing Components and Supplies\",\"service\":false,\"parent\":null}}}},\"UOM\":\"psc.\"},\"supplierId\":\"9ca21bc1-9a17-464f-8bbd-86a15d08277f\",\"supplierName\":\"crow0002\",\"supplierPublic\":false}]},\"facets\":[]}";
        NSData *jsonData = [ret dataUsingEncoding:NSUTF8StringEncoding];
        [self processSuggestionsResponse:jsonData];
        return;
    }
    
    NSURLSession *sess = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [sess dataTaskWithRequest:[self prepareApiRequestWithLimit:limit withPhrase:phrase] completionHandler:^(NSData *data, NSURLResponse *resp, NSError *err){
        if (err != nil) {
            printf("error\n");
        }
        if (resp == nil) {
            printf("no response\n");
        } else {
            printf("received\n\n");
            [self processSuggestionsResponse:data];
        }
    }];
    [task resume];
}

- (NSMutableURLRequest*)prepareApiRequestWithLimit:(int)limit withPhrase:(NSString*)phrase {
    if (apiRequest == nil) {
        apiRequest = [[NSMutableURLRequest alloc] init];
        apiRequest.HTTPMethod = @"GET";
    }
    //TODO form proper URL string &query={%%22q%%22:%%22%@%%22}
    NSString *urlString = [NSString stringWithFormat:@"%@?limit=%d&offset=0&q=%@",apiUrl, limit, phrase];
    printf("urlString: %s\n", [urlString UTF8String]);
    apiRequest.URL = [NSURL URLWithString:urlString];
    return apiRequest;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
